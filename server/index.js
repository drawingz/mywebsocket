const ws = require('ws');

;(() => {
  const server = new ws.Server({
    port: 5211
  })

  const init = () => {
    bindEvent();
  }

  function bindEvent () {
    server.on('open', handleOpen);
    server.on('close', handleClose);
    server.on('error', handleError);
    server.on('connection', handleConnection);
  }

  function handleOpen () {

  }

  function handleClose () {
    
  }

  function handleError () {
    
  }

  function handleConnection (ws) {
    ws.on('message', handleMessage);
  }

  function handleMessage (msg) {
    console.log(msg)
    server.clients.forEach(item => {
      item.send(msg);
    })
  }

  init();
})();