;(() => {
  const oUserName = document.querySelector('#user_name');
  const oLoginBtn = document.querySelector('#login_btn');

  const init = () => {

  }

  function bindEvent () {
    oLoginBtn.addEventListener('click', handleLoginBtnClick, false);
    oLoginBtn.addEventListener('click', handleLoginBtnClick, false);
  }

  function handleLoginBtnClick () {
    const username = oUserName.value.trim();

    if (username.length < 6) {
      alert('用户名需要大于6位')
      return;
    }

    localStorage.setItem('username', username);
    location.href = 'index.html';
  }

  init();
})()